# Установка

---

Основной материял по использованию контейнеров можно найти в [модуле 5](https://gitlab.com/drone-programming/skillbox/module_5/-/blob/master/docs/docker_usage.md?ref_type=heads)
Если контейнер уже был установлен можно перейти к [запуску](#запуск)

---


```bash
curl -fsSL https://get.docker.com | sh
```

```bash
sudo usermod -aG docker $USER
```

```bash
sudo systemctl disable --now docker.service docker.socket
```

```bash
sudo apt-get install -y uidmap
```

```bash
dockerd-rootless-setuptool.sh install --force
```


## NVIDIA Container Toolkit 

[Инструкция с оффициального сайта](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/install-guide.html)

```bash
curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
  && curl -s -L https://nvidia.github.io/libnvidia-container/stable/deb/nvidia-container-toolkit.list | \
    sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
    sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list
```

```bash
sudo apt update
sudo apt install -y nvidia-container-toolkit
```

```bash
sudo nvidia-ctk runtime configure --runtime=docker
```

```bash
sudo systemctl restart docker
```

# Загрузка контейнера 



```bash
docker login registry.gitlab.com
```

```bash
docker pull registry.gitlab.com/drone-programming/skillbox/module_7/px4_simulation:latest
```


В качестве альтернативы можно самостоятельно выполнить сборку.

Для сборки контейнера необходимо выполнить команду:

```bash
docker build -t px4_simulation:latest .
```

# Запуск


Для запуска предлагается воспользоваться скриптом из папки scripts


```bash
./scripts/start_sim_docker.sh
```

или, в случае самостоятельной сборки контейнера:

```bash
./scripts/start_sim_docker.sh -c px4_simulation:latest
```

При необходимости использования GPU добавте флаг '-u 1'

```bash
./scripts/start_sim_docker.sh -u 1
```

# Типовые ошибки

В случае подобной ошибки выполните установку [NVIDIA Container Toolkit](#nvidia-container-toolkit)

```bash
docker: Error response from daemon: unknown or invalid runtime name: nvidia.
```         

В случае если у вас отсутствует GPU от nvidia или же имеются проблемы с драйвером использовать флаг '-u 1' не нужно.