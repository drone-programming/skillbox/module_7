#!/bin/bash

# Setup environment
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
echo $ROS_MASTER_URI
MODEL="uav_camera_lidar" # uav_lidar - only 2d lidar, uav_vision - only depth camera
RVIZ_ENABLE=true
WORLD="mapping_test" # mapping_test - simple objects for test, mapping_village - more complicated environment with trees an house models around
roslaunch $SCRIPT_DIR/../launch/uav_simulator.launch \
          sdf:=$SCRIPT_DIR/../sim/models/$MODEL/$MODEL.sdf \
          world:=$SCRIPT_DIR/../sim/worlds/$WORLD.world \
          script_directory:=$SCRIPT_DIR \
          rviz_enable:=$RVIZ_ENABLE
