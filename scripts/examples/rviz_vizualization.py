# Импорт необходимых библиотек
import rospy
import numpy as np
import time
from visualization_msgs.msg import Marker, MarkerArray
import geometry_msgs.msg

# Инициализация ROS ноды с именем 'voxel_visualization'
rospy.init_node('voxel_visualization')

# Создание публикатора для отправки сообщений типа MarkerArray на топик 'voxel_marker_array'
pub = rospy.Publisher('voxel_marker_array', MarkerArray, queue_size=10)

# Инициализация массива маркеров
marker_array = MarkerArray()

# Создание маркера для визуализации вокселей
marker = Marker()
marker.header.frame_id = "map"  # Используемая карта координат
marker.id = 0  # Уникальный идентификатор маркера
marker.type = Marker.CUBE_LIST  # Тип маркера - массив кубиков (вокселей)
marker.action = Marker.ADD  # Действие - добавить маркер

# Установка ориентации маркера в формате кватерниона
marker.pose.orientation.x = 0.0
marker.pose.orientation.y = 0.0
marker.pose.orientation.z = 0.0
marker.pose.orientation.w = 1.0

# Установка размера вокселя
marker.scale.x = 0.1
marker.scale.y = 0.1
marker.scale.z = 0.1

# Установка цвета вокселя (зеленый с некоторой прозрачностью)
marker.color.r = 0.0
marker.color.g = 1.0
marker.color.b = 0.0
marker.color.a = 0.7

# Генерация позиций вокселей (просто пример)
for i in range(10):
    for j in range(10):
        point = geometry_msgs.msg.Point()
        point.x = np.cos(i) * 2 + np.sin(j) * 2
        point.y = np.cos(i) * 2
        point.z = np.cos(j) * 2 - np.sin(i) * 2
        marker.points.append(point)  # Добавление позиции вокселя в массив

# Добавление маркера в массив маркеров
marker_array.markers.append(marker)
# В этот массив можно добавить и другие маркеры. 

# Бесконечный цикл публикации массива маркеров
while(True):
    pub.publish(marker_array)
    time.sleep(1)  # Пауза в 1 секунду между публикациями
