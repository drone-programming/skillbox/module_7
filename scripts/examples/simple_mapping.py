import time
import numpy as np
from threading import Thread

import pcl
import rospy
import geometry_msgs.msg
import sensor_msgs.point_cloud2 as pc2
from visualization_msgs.msg import Marker, MarkerArray
from sensor_msgs.msg import PointCloud2

import tf2_ros
import tf2_py as tf2
from tf2_sensor_msgs.tf2_sensor_msgs import do_transform_cloud

class VoxelVisualizer:
    def __init__(self, r:float = 0, g:float = 1.0, b :float=0,
                 a :float=1, size: float = 0.5):
        # Создание публикатора для отправки сообщений типа MarkerArray на топик 'voxel_marker_array'
        self._pub = rospy.Publisher('voxel_marker_array', MarkerArray, queue_size=10)

        # Инициализация массива маркеров
        self.marker_array = MarkerArray()

        # Создание маркера для визуализации вокселей
        self.marker = Marker()
        self.marker.header.frame_id = "map"  # Используемая карта координат
        self.marker.id = 0  # Уникальный идентификатор маркера
        self.marker.type = Marker.CUBE_LIST  # Тип маркера - массив кубиков (вокселей)
        self.marker.action = Marker.ADD  # Действие - добавить маркер

        # Установка ориентации маркера в формате кватерниона
        self.marker.pose.orientation.x = 0.0
        self.marker.pose.orientation.y = 0.0
        self.marker.pose.orientation.z = 0.0
        self.marker.pose.orientation.w = 1.0
        
        # Установка размера вокселя
        self.marker.scale.x = size
        self.marker.scale.y = size
        self.marker.scale.z = size

        # Установка цвета вокселя (зеленый с некоторой прозрачностью)
        self.marker.color.r = r
        self.marker.color.g = g
        self.marker.color.b = b
        self.marker.color.a = a
        
        self.marker_array.markers.append(self.marker)
    
    def clear(self):
        self.marker.points.clear()
    
    def add_marker(self, x :float, y :float, z :float):
        point = geometry_msgs.msg.Point()
        point.x = x
        point.y = y
        point.z = z
        self.marker.points.append(point)
        
    def send_update(self):
        self._pub.publish(self.marker_array)



class CircularArray:
    def __init__(self, size):
        self.size = size
        self.data = [None] * size
        self.index = 0

    def push(self, value):
        """Add an element to the circular array."""
        self.data[self.index] = value
        self.index = (self.index + 1) % self.size

    def __getitem__(self, key):
        """Retrieve an element from the circular array."""
        return self.data[(self.index + key) % self.size]

    def __iter__(self):
        """Return an iterator object."""
        self.iterator_index = 0
        return self

    def __next__(self):
        """Return the next element in the circular array."""
        if self.iterator_index >= self.size:
            raise StopIteration
        else:
            value = self.data[(self.index + self.iterator_index) % self.size]
            self.iterator_index += 1
            return value

    def __repr__(self):
        """Return a string representation of the circular array."""
        return str(self.data)


class SimpleMapping:
    def __init__(self) -> None:
        # Выполняем подписку на облако точек
        rospy.Subscriber('/realsense_d435_depth/points', PointCloud2, self.point_cloud_callback)
        self.points_map  = None
        self._visualization = True
        # Устанавливаем размер буфера для карты.
        # Кольцевой буфер будет обновлять старые элементы при заполнении новыми
        # если будет заполнен, то есть заполняем как бы по кругу.
        self._map = CircularArray(3000)
        # Создаем визуализатор
        self._visualizer = VoxelVisualizer()
        # Создаем объект потока для визуализации
        self._mappng = Thread(target=self.mapping_cycle)
        
        # Создаем объект буфера, который хранит положение и ориентацию всех систем координат
        # организованных на нашем аппарате, в течении некоторого времени. Эти данные
        # нужны для того чтобы переводить данные различных датчиков в нужные там системы координат
        self.tf_buffer = tf2_ros.Buffer()
        # Подписываемся на данные о положении и ориентации систем координат
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer)
    
    def run_mapping(self):    
        self._mappng.run()
        
    def mapping_cycle(self):
        rate = rospy.Rate(1)
        while (not rospy.is_shutdown()):
            self.update_map()
            rate.sleep()
        
    def point_cloud_callback(self, msg):
        # Преобразование облака точек в формат PCL
        cloud = pc2.read_points(msg, field_names=("x", "y", "z"), skip_nans=True)
        pcl_cloud = pcl.PointCloud()
        pcl_cloud.from_list(list(cloud))
        # Проверка на существование облака точек
        if pcl_cloud is None:
            return
        # Обрезка облака точек по максимальной и минимальной дальности
        # В библиотеке pcl создается специальный экземпляр фильтра.
        clip_filter = pcl_cloud.make_passthrough_filter() 
        # Установка максимального и минимального значения дальности 
        clip_filter.set_filter_limits(0.1, 7.0)
        # Установка оси по которой будет осуществляться фильтрация
        clip_filter.set_filter_field_name("z") 
        # Вызов метода фильтрации
        clipped_cloud = clip_filter.filter()
        
        # Создание фильтра для усреднения облака точек с уменьшением разрешения
        voxel_filter = clipped_cloud.make_voxel_grid_filter()
        # Устанавливаем размер области усреднения
        voxel_filter.set_leaf_size(1, 1, 1)
        # Выполняем уменьшение разрешение облака точек(можно сравнить с уменьшением разрешения изображения)
        downsampled_points = voxel_filter.filter()
        
        # Выполняем перевод облака точек обратно в формат ros для перевод в систему координат карты
        ros_cloud = pc2.create_cloud_xyz32(msg.header, downsampled_points.to_list())
        # Далее выполняется перевод в систему координат карты
        try:
            # получаем объект трансформации(по сути матрицу поворотов и вектор смещения), для перехода из системы координат камеры глубины
            # в систему координат карты("map"). Обязательно передаем в качестве аргумента метку времени сообщения (msg.header.stamp)
            # в противном случае мы можем получить матрицу и смещения для другого момента времени, не связанного с сообщением, и получим
            # облако точек не в том месте на карте где оно было получено. rospy.Duration(4.0) это время в течении которого сы ждем получение
            # преобразования, в противном случае получаем ошибку.
            trans = self.tf_buffer.lookup_transform("map", msg.header.frame_id, msg.header.stamp, rospy.Duration(4.0))
            # Выполняем переход облака точек между системами координат.
            transformed_cloud_ros = do_transform_cloud(ros_cloud, trans)
        except (tf2.LookupException, tf2.ConnectivityException, tf2.ExtrapolationException) as e:
            rospy.logerr(e)
            return
        
        # Выполняем обратное преобразование облака точек формата ros, в формат списка с которым будем работать далее
        self.points_map  = list(pc2.read_points(transformed_cloud_ros, field_names=("x", "y", "z"), skip_nans=True))

    def update_visualization(self):
        # очищаем текущий массив карты
        self._visualizer.clear()
        #  добавляем все точки в массив и отправляем его
        for elem in self._map.data:
            if elem is None:
                continue
            self._visualizer.add_marker(elem[0],elem[1],elem[2])
        self._visualizer.send_update()
        
    def update_map(self):
        # проверяем доступность точек
        if self.points_map is None:
            return
        
        # добавляем новые точки в буфер
        for elem in self.points_map:
            self._map.push(elem)
        
        # Выполняем обновление карты
        if self._visualization:
            self.update_visualization()
            
            
        
if __name__ == "__main__":
    # Инициализируем ноду
    rospy.init_node('simple_mapping')
    # Создаем класс построения карты
    map_builder = SimpleMapping()
    # Запускаем поток с обновлением карты
    map_builder.run_mapping()
    # Запускаем поток обновления событий ros.
    rospy.spin()
   